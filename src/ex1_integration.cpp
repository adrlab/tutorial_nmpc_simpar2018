/*!
 *
 * ex1_integration.cpp
 */

#include <ct/core/core.h>
#include <tutorial_nmpc_simpar2018/CartPole.h>
#include <tutorial_nmpc_simpar2018/PlotHelper.h>

using ct::models::CartPole;

// this function simulates a cartpole
int main(int argc, char** argv)
{
    // cart pole state dimensions
    const size_t state_dim = CartPole<double>::nStates;  // = 4

    // create a state (ct::core::StateVector) of correct size...
    ct::core::StateVector<state_dim> x;

    // ... and initialize it to zero (setZero)

    // perturb the initial angle (index 1) to 3.0

    // create a std::shared_ptr of our cart pole instance with scalar type double (CartPole<double>())

    // create an integrator (ct::core::Integrator) with correct state dimension
    // and pass the cartpole shared_ptr in the constructor

    // simulate settings
    double dt = 0.001;
    ct::core::Time t0 = 0.0;
    size_t nSteps = 3000; // simulate 3 seconds

    // Plot helper
    PlotHelper helper;

    auto start = std::chrono::high_resolution_clock::now();

    for(size_t i=0; i<nSteps; i++) {
        // call integrate_n_steps of the integrator here to simulate for 1 step of size dt
        helper.plot(x);
        std::this_thread::sleep_until(start + std::chrono::duration<double>(i*dt));
    }

    return 0;
}
