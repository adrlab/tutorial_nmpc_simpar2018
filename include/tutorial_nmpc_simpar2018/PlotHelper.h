/**********************************************************************************************************************
This file is part of the Control Toolbox (https://adrlab.bitbucket.io/ct), copyright by ETH Zurich, Google Inc.
Licensed under Apache2 license (see LICENSE file in main directory)
**********************************************************************************************************************/

#pragma once

#include <iostream>
#include <map>

#include <ct/core/core.h>
#include "CartPole.h"

class PlotHelper {
public:

  enum Symbol {
    EMPTY = 0,
    RAIL = 1,
    CART = 2,
    POLE = 3,
  };

  PlotHelper(size_t nRows = 40, size_t nCols = 40, double scale = 10) :
    nRows_(nRows),
    nCols_(nCols),
    scale_(scale)
 {
     symbol_map_[Symbol::EMPTY] = ' ';
     symbol_map_[Symbol::RAIL] = '-';
     symbol_map_[Symbol::CART] = '#';
     symbol_map_[Symbol::POLE] = '*';

     draw_.resize(nRows, nCols);
  }


  template <typename SCALAR>
  void plot(const ct::core::StateVector<ct::models::CartPole<SCALAR>::nStates, SCALAR>& state)
  {
    draw_.fill(static_cast<int>(Symbol::EMPTY));
    size_t mid_row = std::round(nRows_/2.0+0.5);
    draw_.row(mid_row).fill(static_cast<int>(Symbol::RAIL));
    int cart_pos = std::round(nCols_/2.0 + scale_*state(0));
    for (int pos=cart_pos-1; pos<cart_pos+2; pos++)
      if (pos>0 && pos<nCols_)
        draw_(mid_row, pos) = Symbol::CART;

    double step = 0.001*scale_;
    double l = 1.2*scale_;
    for (double ll=0; ll<l; ll+=step)
    {
      int x = std::round(cart_pos + ll*std::sin(state(1)));
      int y = std::round(ll*std::cos(state(1)) + mid_row);
      if ((x==cart_pos && y==mid_row) || y>=nCols_ || y<0 || x<0 || x>=nRows_)
        continue;
      draw_(y,x) = static_cast<int>(Symbol::POLE);
    }

    std::cout<< "\x1B[2J\x1B[H";
    for(size_t r=0; r<nRows_; r++) {
      for(size_t c=0; c<nCols_; c++) {
        std::cout<<symbol_map_[static_cast<Symbol>(draw_(r,c))];
      }
      std::cout<<std::endl;
    }
  }

  private:
    size_t nRows_;
    size_t nCols_;
    double scale_;
    std::map<Symbol, char> symbol_map_;
    Eigen::MatrixXi draw_;

};
