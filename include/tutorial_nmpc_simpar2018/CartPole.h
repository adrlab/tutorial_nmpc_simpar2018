/**********************************************************************************************************************
This file is part of the Control Toolbox (https://adrlab.bitbucket.io/ct), copyright by ETH Zurich, Google Inc.
Licensed under Apache2 license (see LICENSE file in main directory)
**********************************************************************************************************************/

#pragma once

#include <iostream>
#include <memory>

#include <ct/core/core.h>


namespace ct {
namespace models {

// we template the class on the scalar type to provide Auto-Diff compatability
template <typename SCALAR>
class CartPole : public ct::core::ControlledSystem<4, 1, SCALAR>
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    static const size_t nStates = 4;  // cartpole has four states
    static const size_t nControls = 1;  // cartpole has force on cart as control

    // default constructor
    CartPole(
      std::shared_ptr<ct::core::Controller<nStates, nControls, SCALAR>> controller = nullptr,
      double m_cart = 1.5, double m_pendulum = 1.2, double l = 0.9) : 
        m_cart_(m_cart),
        m_pendulum_(m_pendulum),
        l_(l),
        ct::core::ControlledSystem<nStates, nControls, SCALAR>(controller)
    {
    }

    // copy constructor
    CartPole(const CartPole& arg) : 
      g_(arg.g_),
      m_cart_(arg.m_cart_),
      m_pendulum_(arg.m_pendulum_),
      l_(arg.l_),
      ct::core::ControlledSystem<nStates, nControls, SCALAR>(arg)
    {}

    // clone function calls copy constructor
    virtual CartPole* clone() const override { return new CartPole(*this); }

    // our actual system dynamics dot{x} = f(x,u)
    virtual void computeControlledDynamics(const ct::core::StateVector<nStates, SCALAR>& state,
        const SCALAR& t, // ignored since not time dependent
        const ct::core::ControlVector<nControls, SCALAR>& control,
        ct::core::StateVector<nStates, SCALAR>& derivative) override
    {
        // convenience accessors
        const SCALAR& x = state(0); // cart position
        const SCALAR& q = state(1); // pole angle
        const SCALAR& x_dot = state(2); // cart velocity
        const SCALAR& q_dot = state(3); // pole angular velocity

        SCALAR& dx = derivative(0);  // time-derivative of cart position
        SCALAR& dq = derivative(1);  // time-derivative of pole angle

        // time derivatives of position are just velocities, so assign them here

        // our system dynamics are M ddot{q} = F
        // create M (2x2) and F(2x1) with SCALAR type

        // compute ddot{q} = M^-1 F and replace the call below
	derivative.setZero();
    }

    const SCALAR& m_cart() const { return m_cart_;}
    const SCALAR& m_pendulum() const { return m_pendulum_;}
    const SCALAR& l() const { return l_;}

private:
   const SCALAR g_ = SCALAR(9.81); // gravity constant
   SCALAR m_cart_; // mass of the cart
   SCALAR m_pendulum_; // mass of the pendulum
   SCALAR l_; // length of the pendulum
};

} // namespace models
} // namespace ct
